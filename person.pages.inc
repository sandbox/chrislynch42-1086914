<?php
// $Id$

/**
 * @file
 * Person module page include
 *
 */

/**
 * A list of all persons.  Can be limited by to those beginning with a
 * specific letter.
 * @global object $user Information about the person logged in.
 * @param string $theletter Letter to limit results with.
 * @return string Rendered HTML.
 */
function person_page_list($theletter=NULL) {
  global $user;
  $output = '';
  $items = array();
  variable_set('person_breadcrumb' , array(l(t('Home') , NULL) , l(t('Person') , 'person')));

  $result = db_query("SELECT DISTINCT substr(replace(upper(n.title) , 'THE' , '') , 1 , 1) theLetter FROM {node} n WHERE n.type = 'person' ORDER BY theLetter");
  $output .= l('[' . t('ALL') .']' , 'person');
  while ($node = db_fetch_object($result)) {
    $output .= l('[' . $node->theLetter . ']' , 'person/'. $node->theLetter);
    $has_posts = TRUE;
  }


  if (user_access('create person nodes')) {
    $items[] = l(t('Create new person entry.') , "node/add/person");
    $output .= theme('item_list' , $items);
  }

  if ($theletter) {
    $result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'person' AND substr(replace(upper(n.title) , 'THE' , '') , 1 , 1) = '%s' ORDER BY n.title , n.sticky DESC , n.created DESC" , $theletter);
  }
  else {
    $result = db_query("SELECT n.nid FROM {node} n WHERE n.type = 'person' ORDER BY n.title , n.sticky DESC , n.created DESC");
  }

  $has_posts = FALSE;

  while ($record = db_fetch_object($result)) {
    $loaded_node = node_load($record->nid);
    $output .= node_view($loaded_node , TRUE , FALSE , TRUE);
    $has_posts = TRUE;
  }

  if ($has_posts) {

  }
  else {
    drupal_set_message(t('No person entries have been created.'));
  }
  return $output;
}
