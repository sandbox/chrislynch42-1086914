<?php
// $Id$

/**
 * @file
 * Person module form include
 *
 */

/**
 * Function to build a form definition array.  Placed in a seperate method
 * so that other modules can call this function to build a form and not repeat
 * code.
 * @param object $node Node object being edited.
 * @return array Form definition
 */
function person_make_form(&$node) {
  global $user;
  $type = node_get_types('type' , $node);


  $form['personset'] = array(
    '#type' => 'fieldset' ,
    '#title' => t('Person') ,
    '#weight' => -50 ,
    '#collapsible' => FALSE ,
    '#collapsed' => FALSE ,
  );

  $form['person_id'] = array(
    '#access' => user_access('create person nodes') ,
    '#type' => 'hidden' ,
    '#default_value' => isset($node->person_id) ?  $node->person_id : 0,
    '#weight' => -20
  );



  $form['personset']['last_name'] = array(
    '#type' => 'textfield' ,
    '#title' => t('Last Name') ,
    '#maxlength' => 60 ,
    '#required' => TRUE ,
    '#default_value' => isset($node->last_name) ?  $node->last_name : ''

  );

  $form['personset']['first_name'] = array(
    '#type' => 'textfield' ,
    '#title' => t('First Name') ,
    '#maxlength' => 60 ,
    '#default_value' => isset($node->first_name) ?  $node->first_name : ''
  );

  $form['personset']['middle_name'] = array(
    '#type' => 'textfield' ,
    '#title' => t('Middle Name') ,
    '#maxlength' => 60 ,
    '#default_value' => isset($node->middle_name) ?  $node->middle_name : ''
  );

  $form['personset']['suffix_name'] = array(
    '#type' => 'textfield' ,
    '#title' => t('Suffix Name') ,
    '#maxlength' => 14 ,
    '#default_value' => isset($node->suffix_name) ?  $node->suffix_name : ''
  );

  $form['personset']['body_filter']['body'] = array(
      '#access' => user_access('create person nodes') ,
      '#type' => 'textarea' ,
      '#title' => t('Describe Person') ,
      '#default_value' => isset($node->body) ?  $node->body : '' ,
      '#required' => FALSE ,
      '#weight' => -19
    );


  $form['title'] = array(
    '#type' => 'hidden' ,
    '#default_value' => ''
  );


  $form['theletter'] = array(
    '#type' => 'hidden' ,
    '#default_value' => isset($node->last_name) ? drupal_strtoupper(drupal_substr($node->last_name , 0 , 1)) : ''
  );

  $form['#submit']=array('person_form_submit');

  return $form;
}
